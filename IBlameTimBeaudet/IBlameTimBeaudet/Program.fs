﻿open System
open IBlameTimBeaudet

[<EntryPoint>]
let main _ =
    Splash.Show()
    MainMenu.Run()
    Outro.Show()
    0
