﻿namespace IBlameTimBeaudet

open System

module CommandSource =
    let ReadCommand () : string list =
        let oldColor = Console.ForegroundColor
        Console.WriteLine()
        Console.ForegroundColor <- ConsoleColor.Gray
        Console.Write ">"
        let result = 
            Console.ReadLine()
                .ToLower()
                .Split([|' '|])
            |> List.ofArray
        Console.ForegroundColor <- oldColor
        result

