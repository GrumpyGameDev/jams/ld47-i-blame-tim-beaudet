﻿namespace IBlameTimBeaudet

open System

module Help =
    let Show() =
        MessageSink.WriteLine ConsoleColor.White ""
        MessageSink.WriteLine ConsoleColor.Magenta "Help:"

        MessageSink.Write ConsoleColor.Gray "    give sammich"
        MessageSink.WriteLine ConsoleColor.DarkGray " - gives a sammich to someone"
        
        MessageSink.Write ConsoleColor.Gray "    give up"
        MessageSink.WriteLine ConsoleColor.DarkGray " - ends the game session and return you to the main menu"

        MessageSink.Write ConsoleColor.Gray "    grill <item>"
        MessageSink.WriteLine ConsoleColor.DarkGray " - grills an item"

        MessageSink.Write ConsoleColor.Gray "    help"
        MessageSink.WriteLine ConsoleColor.DarkGray " - shows help on command (you are here)"

        MessageSink.Write ConsoleColor.Gray "    inventory"
        MessageSink.WriteLine ConsoleColor.DarkGray " - shows yer inventory"

        MessageSink.Write ConsoleColor.Gray "    make sammich"
        MessageSink.WriteLine ConsoleColor.DarkGray " - makes a sammich (alternately, you can say 'sandwich')"

        MessageSink.Write ConsoleColor.Gray "    open door"
        MessageSink.WriteLine ConsoleColor.DarkGray " - opens a door"

        MessageSink.Write ConsoleColor.Gray "    talk"
        MessageSink.WriteLine ConsoleColor.DarkGray " - talks to someone"

        MessageSink.Write ConsoleColor.Gray "    take <item>"
        MessageSink.WriteLine ConsoleColor.DarkGray " - takes an item"

        MessageSink.Write ConsoleColor.Gray "    turn around"
        MessageSink.WriteLine ConsoleColor.DarkGray " - turns you around"

        MessageSink.Write ConsoleColor.Gray "    unlock door"
        MessageSink.WriteLine ConsoleColor.DarkGray " - unlocks a door"

        MessageSink.Write ConsoleColor.Gray "    walk"
        MessageSink.WriteLine ConsoleColor.DarkGray " - walks for a short distance"

