﻿namespace IBlameTimBeaudet

open System

module Give =
    let Handle (world:World) : World =
        if World.HasFeature (Cyclops Key) world then
            match World.GetSammich world with
            | Some sammich ->
                let world =
                    world
                    |> World.AddInventory sammich -1
                let world, messages =
                    world
                    |> World.GiveSammich sammich
                messages
                |> List.iter
                    (fun x -> x ||> MessageSink.WriteLine)
                world
            | _ ->
                Music.Play Songs.Error
                MessageSink.BlankLine()
                MessageSink.WriteLine ConsoleColor.Red "You have no sammich to give!"
                world
        else
            Music.Play Songs.Error
            MessageSink.BlankLine()
            MessageSink.WriteLine ConsoleColor.Red "There is no one around to give it to!"
            world

