﻿namespace IBlameTimBeaudet

open System

module Outro =
    let Show() : unit =
         MessageSink.WriteLine ConsoleColor.Cyan "Thank you for playing!"
         Music.Play Songs.Outro

