﻿namespace IBlameTimBeaudet

open System

module Instructions =
    let Show() =
        MessageSink.WriteLine ConsoleColor.Magenta ""
        MessageSink.WriteLine ConsoleColor.Magenta "How To Play:"
        MessageSink.WriteLine ConsoleColor.DarkGray "You are Tim Beaudet, and you are on a path."
        MessageSink.WriteLine ConsoleColor.DarkGray "Somewhere on this path is a door."
        MessageSink.WriteLine ConsoleColor.DarkRed "Beware! The path is not what it seems! Going back might wind you up somewhere very different than going forward."
        MessageSink.WriteLine ConsoleColor.DarkGray "Your job is to find the door and  go through it."

