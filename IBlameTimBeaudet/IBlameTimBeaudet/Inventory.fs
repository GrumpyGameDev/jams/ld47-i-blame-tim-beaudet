﻿namespace IBlameTimBeaudet

open System

module Inventory =
    let Show(world:World) = 
        MessageSink.BlankLine()
        MessageSink.WriteLine ConsoleColor.Magenta "You are carrying:"
        if world.inventory.IsEmpty then
            MessageSink.WriteLine ConsoleColor.DarkGray "    Nothing"
        else
            world.inventory
            |> Map.iter
                (fun item quantity ->
                    MessageSink.Write ConsoleColor.Yellow (sprintf "    %dx " quantity)
                    MessageSink.WriteLine ConsoleColor.DarkCyan (item |> Feature.GetName))

