﻿namespace IBlameTimBeaudet

open System

module MessageSink =
    let Write
            (color: ConsoleColor)
            (text: string)
            : unit =
        Console.ForegroundColor <- color
        Console.Write text

    let WriteLine
            (color: ConsoleColor)
            (text: string)
            : unit =
        Console.ForegroundColor <- color
        Console.WriteLine text

    let BlankLine () = Console.WriteLine()

