﻿namespace IBlameTimBeaudet

[<AutoOpen>]
module Notes =
    let C4  = 261
    let CS4 = 277
    let D4  = 293
    let DS4 = 311
    let E4  = 329
    let F4  = 349
    let FS4 = 369
    let G4  = 392
    let GS4 = 415
    let A4  = 440
    let AS4 = 466
    let B4  = 493
    let C5  = 523
    let CS5 = 554
    let D5  = 587
    let DS5 = 622
    let E5  = 659
    let F5  = 698
    let FS5 = 739
    let G5  = 783
    let GS5 = 830
    let A5  = 880
    let AS5 = 932
    let B5  = 987
    let C6  = 1046

module Songs =
    let Error =
        [
            100, 400
        ]

    let Win =
        [
            C5, 200
            D5, 200
            E5, 200
            F5, 200
            G5, 200
            A5, 200
            B5, 200
            C6, 800
        ]

    let Intro =
        [
            C4, 200
            E4, 200
            A4, 200
            C5, 200
        ]

    let Outro =
        [
            C5, 200
            A4, 200
            E4, 200
            C4, 200
        ]

    let Lettuce =
        [
            D5, 200
            E5, 200
            FS5, 200
            D5, 200
        ]

    let Tomato =
        [
            G5, 200
            A5, 200
            B5, 200
            G5, 200
        ]

    let Bread = 
        [
            E5, 200
            CS5, 200
            B4, 200
            A4, 200
        ]

    let Bacon = 
        [
            A5, 200
            A4, 200
            B4, 200
            CS5, 200
        ]

    let Unlock = 
        [
            G5, 200
            D5, 200
            G5, 400
        ]

    let Grill =
        [
            G5, 200
            FS5, 200
            E5, 200
            D5, 200
        ]

    let Make =
        [
            C5, 200
            D5, 200
            E5, 200
            FS5, 200
            G5, 800
        ]

    let Talk =
        [
            D5, 200
            A4, 200
            D5, 400
        ]

    let Give =
        [
            D5, 200
            G5, 200
            D5, 400
        ]


