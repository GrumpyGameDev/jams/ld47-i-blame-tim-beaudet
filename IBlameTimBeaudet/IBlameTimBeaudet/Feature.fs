﻿namespace IBlameTimBeaudet

type Feature =
    | Door of bool
    | Key
    | Cyclops of Feature
    | Bread
    | Tomato
    | Lettuce
    | Bacon
    | Sammich of Map<Feature, int>
    | Grilled of Feature
    | Source of Feature
    | Griller

module Feature =
    let rec GetName = 
        function
        | Cyclops Key ->
            "a hungry looking cyclops with a key hung around their neck"
        | Cyclops _ ->
            "a cyclops messily eating a sammich"
        | Griller ->
            "a geothermically heated flat rock, suitable for grilling"
        | Source Tomato ->
            "a sliced tomato bush"
        | Tomato ->
            "tomato slice"
        | Source Bread ->
            "a sliced bread tree"
        | Bread ->
            "bread slice"
        | Source Lettuce ->
            "a lettuce vine"
        | Lettuce ->
            "lettuce leaf"
        | Source Bacon ->
            "a patch of bacon flowers"
        | Bacon ->
            "slice of raw bacon"
        | Grilled Bacon ->
            "a slice of grilled bacon"
        | Door _ ->
            "a door"
        | Key ->
            "key"
        | Sammich _ ->
            "sammich"
        | Grilled x ->
            x |> GetName |> sprintf "grilled %s"
        | x ->
            sprintf "***%A***" x

    let rec IsSammich (feature:Feature) : bool =
        match feature with
        | Grilled x ->
            x |> IsSammich
        | Sammich _ ->
            true
        | _ -> 
            false

    let IsGrilled (feature:Feature) : bool =
        match feature with
        | Grilled _ -> true
        | _ -> false

    let IsIngredient (feature:Feature) : bool =
        match feature with
        | Grilled Bacon
        | Bread
        | Tomato
        | Lettuce ->
            true
        | _ ->
            false

    let IsCyclops (feature:Feature) : bool =
        match feature with
        | Cyclops _ -> true
        | _ -> false

    let rec GetIngredients (feature:Feature) : Map<Feature, int> =
        match feature with
        | Grilled x -> x |> GetIngredients
        | Sammich x -> x
        | _ -> Map.empty
