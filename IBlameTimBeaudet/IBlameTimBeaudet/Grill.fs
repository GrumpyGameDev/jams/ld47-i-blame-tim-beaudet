﻿namespace IBlameTimBeaudet

open System

module Grill =
    let HandleBacon (world:World) : World =
        if World.HasFeature Griller world then
            if World.HasItem Bacon world then
                Music.Play Songs.Grill
                MessageSink.BlankLine()
                MessageSink.WriteLine ConsoleColor.Green "You grill up a slice of bacon!"
                world
                |> World.GrillBacon
            else
                Music.Play Songs.Error
                MessageSink.BlankLine()
                MessageSink.WriteLine ConsoleColor.Red "You have no raw bacon!"
                world
        else
            Music.Play Songs.Error
            MessageSink.BlankLine()
            MessageSink.WriteLine ConsoleColor.Red "There is no griller here!"
            world

    let HandleSammich (world:World) : World =
        match world |> World.GetSammich with
        | Some sammich ->
            if sammich |> Feature.IsGrilled then
                Music.Play Songs.Error
                MessageSink.BlankLine()
                MessageSink.WriteLine ConsoleColor.Red "The sammich has already been grilled!"
                world
            else
                Music.Play Songs.Grill
                MessageSink.BlankLine()
                MessageSink.WriteLine ConsoleColor.Green "You grill the sammich!"
                world
                |> World.GrillSammich sammich
        | None ->
            Music.Play Songs.Error
            MessageSink.BlankLine()
            MessageSink.WriteLine ConsoleColor.Red "You have no sammich!"
            world


