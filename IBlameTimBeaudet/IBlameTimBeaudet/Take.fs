﻿namespace IBlameTimBeaudet

open System

module Take =
    let private HandleLettuce (world:World) : World =
        if world |> World.HasFeature (Source Lettuce) then
            Music.Play Songs.Lettuce
            MessageSink.WriteLine ConsoleColor.Green "You take some lettuce from the vine."
            world
            |> World.AddInventory Lettuce 1
        else
            Music.Play Songs.Error
            MessageSink.WriteLine ConsoleColor.Red "I don't see any lettuce around here!"
            world

    let private HandleBacon (world:World) : World =
        if world |> World.HasFeature (Source Bacon) then
            Music.Play Songs.Bacon
            MessageSink.WriteLine ConsoleColor.Green "You harvest a bacon flower."
            world
            |> World.AddInventory Bacon 1
        else
            Music.Play Songs.Error
            MessageSink.WriteLine ConsoleColor.Red "I don't see any bacon flowers around here!"
            world

    let private HandleBread (world:World) : World =
        if world |> World.HasFeature (Source Bread) then
            Music.Play Songs.Bread
            MessageSink.WriteLine ConsoleColor.Green "You harvest a bread slice."
            world
            |> World.AddInventory Bread 1
        else
            Music.Play Songs.Error
            MessageSink.WriteLine ConsoleColor.Red "I don't see any bread slices around here!"
            world

    let private HandleTomato (world:World) : World =
        if world |> World.HasFeature (Source Tomato) then
            Music.Play Songs.Tomato
            MessageSink.WriteLine ConsoleColor.Green "You harvest a tomato slice."
            world
            |> World.AddInventory Tomato 1
        else
            Music.Play Songs.Error
            MessageSink.WriteLine ConsoleColor.Red "I don't see any tomato slices around here!"
            world

    let Handle (tokens:string list) (world:World) : World =
        MessageSink.BlankLine()
        match tokens with
        | ["lettuce"] 
        | ["lettuce"; "leaf"] 
        | ["leaf"] ->
            world
            |> HandleLettuce

        | ["bacon"] 
        | ["bacon"; "flower"] 
        | ["flower"] ->
            world
            |> HandleBacon

        | ["bread"] 
        | ["bread"; "slice"] 
        | ["slice";"of";"bread"] ->
            world
            |> HandleBread

        | ["tomato"] 
        | ["tomato"; "slice"] 
        | ["slice";"of";"tomato"] ->
            world
            |> HandleTomato

        | _ ->
            Music.Play Songs.Error
            MessageSink.WriteLine ConsoleColor.Red "I don't know what yer trying to 'take'..."
            world

