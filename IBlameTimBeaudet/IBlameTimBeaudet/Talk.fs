﻿namespace IBlameTimBeaudet

open System

module Talk =
    let Handle (world:World) : World =
        if World.HasFeature (Cyclops Key) world then
            Music.Play Songs.Talk
            MessageSink.BlankLine()
            MessageSink.WriteLine ConsoleColor.Yellow "The cyclops says 'I'd give anything to have a sammich right about now!'"
            world
        else
            match World.GetFeatures world |> List.tryHead with
            | Some (Cyclops _) ->
                Music.Play Songs.Error
                MessageSink.BlankLine()
                MessageSink.WriteLine ConsoleColor.Red "The cyclops is busy eating his sammich, so it is best not to bother them!"
                world
            | _ ->
                Music.Play Songs.Error
                MessageSink.BlankLine()
                MessageSink.WriteLine ConsoleColor.Red "There is no one to talk to here!"
                world

