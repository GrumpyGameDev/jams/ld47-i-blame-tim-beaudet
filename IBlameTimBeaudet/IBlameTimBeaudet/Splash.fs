﻿namespace IBlameTimBeaudet

open System

module Splash =
    let Show() =
        Console.Title <- "I Blame Tim Beaudet"
        MessageSink.WriteLine ConsoleColor.Cyan "Welcome to 'I Blame Tim Beaudet'"
        MessageSink.WriteLine ConsoleColor.DarkCyan "A production of TheGrumpyGameDev"
        MessageSink.WriteLine ConsoleColor.DarkGray "For LD 47, with a theme of 'Stuck in a loop'"
        Music.Play Songs.Intro


