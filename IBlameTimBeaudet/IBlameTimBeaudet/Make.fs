﻿namespace IBlameTimBeaudet

open System

module Make =
    let HandleSammich (world:World) : World =
        let recipe =
            world
            |> World.GetIngredients
        if recipe.IsEmpty then
            Music.Play Songs.Error
            MessageSink.BlankLine()
            MessageSink.WriteLine ConsoleColor.Red "You dont have any ingredients!"
            world
        else
            if recipe |> Map.tryFind Bread |> Option.defaultValue 0 < 2 then
                Music.Play Songs.Error
                MessageSink.BlankLine()
                MessageSink.WriteLine ConsoleColor.Red "You need at least two slices of bread!"
                world
            elif recipe.Count < 2 then
                Music.Play Songs.Error
                MessageSink.BlankLine()
                MessageSink.WriteLine ConsoleColor.Red "You need at least two ingredients!"
                world
            else
                Music.Play Songs.Make
                MessageSink.BlankLine()
                MessageSink.WriteLine ConsoleColor.Green "You make a sammich!"
                world
                |> World.MakeSammich recipe

