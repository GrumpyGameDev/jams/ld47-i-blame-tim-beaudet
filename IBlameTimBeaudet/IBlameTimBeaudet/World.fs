﻿namespace IBlameTimBeaudet

open System

type Facing =
    | Ahead
    | Back


type World =
    {
        moves : int
        position : int
        facing : Facing
        paths : Map<(int * Facing),int>
        features : Map<Feature, int>
        inventory: Map<Feature, int>
    }


module World =
    let HasItem (feature:Feature) (world: World) : bool =
        world.inventory.ContainsKey feature

    let HasKey = HasItem Key

    let AddInventory (feature:Feature) (quantity:int) (world:World) =
        let oldQuantity = 
            world.inventory
            |> Map.tryFind feature
            |> Option.defaultValue 0
        let newQuantity = oldQuantity + quantity
        let inventory = 
            if newQuantity >0 then
                world.inventory
                |> Map.add feature newQuantity
            else
                world.inventory
                |> Map.remove feature
        {world with 
            inventory = inventory}

    let GetFeatures (world:World) : Feature list =
        world.features
        |> Map.filter
            (fun _ v -> v = world.position)
        |> Map.toList
        |> List.map fst

    let HasFeature (feature:Feature) (world:World) : bool =
        world
        |> GetFeatures
        |> List.exists(fun x -> x = feature)

    let TurnAround (world:World) : World =
        match world.facing with
        | Ahead ->
            {world with facing = Back; moves = world.moves + 1}
        | Back ->
            {world with facing = Ahead; moves = world.moves + 1}

    let Walk (world:World) : World =
        world.paths
        |> Map.tryFind (world.position, world.facing)
        |> Option.fold
            (fun w p ->
                {w with position = p}) world

    
    let Create () 
            : World =
        {
            inventory = 
                Map.empty
            moves = 0
            position = 0
            facing = Ahead
            paths = 
                [
                    (0, Ahead), 1
                    (0, Back), 8
                    (1, Ahead), 2
                    (1, Back), 24
                    (2, Ahead), 3
                    (2, Back), 30
                    (3, Ahead), 4
                    (3, Back), 13
                    (4, Ahead), 5
                    (4, Back), 23
                    (5, Ahead), 6
                    (5, Back), 22
                    (6, Ahead), 7
                    (6, Back), 18
                    (7, Ahead), 0
                    (7, Back), 16

                    (8, Ahead), 12
                    (8, Back), 9
                    (9, Ahead), 8
                    (9, Back), 10
                    (10, Ahead), 9
                    (10, Back), 11
                    (11, Ahead), 10
                    (11, Back), 0
                    (12, Ahead), 11
                    (12, Back), 8

                    (13, Ahead), 14
                    (13, Back), 3
                    (14, Ahead), 3
                    (14, Back), 15
                    (15, Ahead), 3
                    (15, Back), 3

                    (16, Ahead), 17
                    (16, Back), 9
                    (17, Ahead), 19
                    (17, Back), 9
                    (18, Ahead), 20
                    (18, Back), 7
                    (19, Ahead), 22
                    (19, Back), 21
                    (20, Ahead), 21
                    (20, Back), 17
                    (21, Ahead), 20
                    (21, Back), 20
                    (22, Ahead), 13
                    (22, Back), 21

                    (23, Ahead), 23
                    (23, Back), 23

                    (24, Ahead), 1
                    (24, Back), 25
                    (25, Ahead), 26
                    (25, Back), 29
                    (26, Ahead), 28
                    (26, Back), 11
                    (27, Ahead), 28
                    (27, Back), 23
                    (28, Ahead), 27
                    (28, Back), 29
                    (29, Ahead), 30
                    (29, Back), 25
                    (30, Ahead), 2
                    (30, Back), 29

                ]
                |> Map.ofList
            features =
                [
                    (Door true), 23
                    (Cyclops Key), 12
                    (Source Bacon), 27
                    (Source Bread), 15
                    (Source Lettuce), 7
                    (Source Tomato), 19
                    (Griller), 21
                ]
                |> Map.ofList
        }

    let RemoveFeature (feature : Feature) (world:World) : World =
        {world with
            features =
                world.features
                |> Map.remove feature}

    let AddFeature (feature:Feature) (position:int) (world:World) : World =
        {world with
            features =
                world.features
                |> Map.add feature position}

    let UnlockDoor (world:World) : World =
        if HasKey world && HasFeature (Door true) world then
            world
            |> AddInventory Key -1
            |> RemoveFeature (Door true)
            |> AddFeature (Door false) world.position
        else
            world

    let GrillBacon (world:World) : World =
        if HasItem Bacon world && HasFeature Griller world then
            world
            |> AddInventory Bacon -1
            |> AddInventory (Grilled Bacon) 1
        else
            world

    let GetSammich (world:World) : Feature option =
        world.inventory
        |> Map.tryPick
            (fun key _ ->
                if key |> Feature.IsSammich then
                    Some key
                else
                    None)

    let GrillSammich (sammich:Feature) (world:World) : World =
        world
        |> AddInventory sammich -1
        |> AddInventory (Grilled sammich) 1

    let GetIngredients (world:World) : Map<Feature, int> =
        world.inventory
        |> Map.filter
            (fun k _ -> k |> Feature.IsIngredient)

    let MakeSammich (ingredients: Map<Feature, int>) (world:World) : World =
        let sammich =
            Sammich ingredients
        ingredients
        |> Map.fold
            (fun w k v -> AddInventory k (-v) w) world
        |> AddInventory sammich 1

    let GiveSammich (sammich:Feature) (world:World) : World * (ConsoleColor * string) list =
        if sammich |> Feature.IsGrilled then
            let ingredients = sammich |> Feature.GetIngredients
            let breadSlices = ingredients |> Map.tryFind Bread |> Option.defaultValue 0
            let bacon = ingredients |> Map.tryFind (Grilled Bacon) |> Option.defaultValue 0
            let tomato = ingredients |> Map.tryFind Tomato |> Option.defaultValue 0
            let lettuce = ingredients |> Map.tryFind Lettuce |> Option.defaultValue 0
            if breadSlices < 3 then
                Music.Play Songs.Error
                world, [(ConsoleColor.DarkYellow, "Not enough bread!")]
            elif breadSlices > 3 then
                Music.Play Songs.Error
                world, [(ConsoleColor.DarkYellow, "Too much bread!")]
            elif bacon <3 then
                Music.Play Songs.Error
                world, [(ConsoleColor.DarkYellow, "Not enough bacon!")]
            elif lettuce < 1 then
                Music.Play Songs.Error
                world, [(ConsoleColor.DarkYellow, "Not enough lettuce!")]
            elif lettuce > 2 then
                Music.Play Songs.Error
                world, [(ConsoleColor.DarkYellow, "Too much lettuce!")]
            elif tomato > 0 then
                Music.Play Songs.Error
                world, [(ConsoleColor.DarkYellow, "Too much tomato!")]
            else
                let world =
                    world
                    |> AddInventory Key 1
                    |> RemoveFeature (Cyclops Key)
                    |> AddFeature (Cyclops sammich) world.position
                Music.Play Songs.Give
                world, [(ConsoleColor.Green, "The cyclops trades you the key for the sammich!")]
        else
            Music.Play Songs.Error
            world, [(ConsoleColor.DarkYellow, "This sammich isn't grilled!")]
