﻿namespace IBlameTimBeaudet

open System
open System.Threading

module Music =
    let Play 
            (song: (int * int) list)
            : unit =
        try
            song
            |> List.iter
                (fun (freq, dur) ->
                    if freq>=32 && freq<32767 then
                        Console.Beep(freq, dur)
                    else
                        Thread.Sleep dur)
        with
        | _ -> () //EAT

