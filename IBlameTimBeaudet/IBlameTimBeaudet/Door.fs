﻿namespace IBlameTimBeaudet

open System

module Door =
    let HandleUnlock (world:World) : World =
        if World.HasKey world then
            if World.HasFeature (Door true) world then
                Music.Play Songs.Unlock
                MessageSink.BlankLine()
                MessageSink.WriteLine ConsoleColor.Green "You unlock the door!"
                world
                |> World.UnlockDoor
            else
                Music.Play Songs.Error
                MessageSink.BlankLine()
                MessageSink.WriteLine ConsoleColor.Red "I see nothing to unlock here!"
                world
        else
            Music.Play Songs.Error
            MessageSink.BlankLine()
            MessageSink.WriteLine ConsoleColor.Red "You have no key!"
            world


    let HandleOpen (world:World) : World option =
        if World.HasFeature (Door true) world then
            Music.Play Songs.Error
            MessageSink.BlankLine()
            MessageSink.WriteLine ConsoleColor.Red "The door is locked!"
            world
            |> Some
        elif World.HasFeature (Door false)  world then
            Music.Play Songs.Win
            MessageSink.BlankLine()
            MessageSink.WriteLine ConsoleColor.Green "You go through the door and escape the path!"
            MessageSink.WriteLine ConsoleColor.Green "You win! Congratulations!"
            world.moves |> sprintf "You completed the game in %d moves" |> MessageSink.WriteLine ConsoleColor.Green 
            None
        else
            Music.Play Songs.Error
            MessageSink.BlankLine()
            MessageSink.WriteLine ConsoleColor.Red "I see no door here!"
            world
            |> Some

