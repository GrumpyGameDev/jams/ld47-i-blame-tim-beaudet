﻿namespace IBlameTimBeaudet

open System

module Game =
    let HandleTurnAround 
            (world :World) : World =
        MessageSink.BlankLine()
        MessageSink.WriteLine ConsoleColor.DarkGray "You turn around."
        world
        |> World.TurnAround

    let HandleWalk
            (world:World) : World =
        MessageSink.BlankLine()
        MessageSink.WriteLine ConsoleColor.DarkGray "You walk for a short distance."
        world
        |> World.Walk


    let rec Run (world: World) =
        MessageSink.BlankLine()
        MessageSink.WriteLine ConsoleColor.Magenta (sprintf "You are on a path at position #%d." world.position)
        world
        |> World.GetFeatures
        |> List.iter
            (Feature.GetName >> sprintf "There is %s here." >> MessageSink.WriteLine ConsoleColor.DarkYellow)

        match CommandSource.ReadCommand() with
        | [ "give" ]
        | [ "give"; "sammich" ]
        | [ "give"; "sandwich" ] ->
            Give.Handle world
            |> Run

        | [ "talk" ] ->
            Talk.Handle world
            |> Run

        | [ "make"; "sammich"]
        | [ "make"; "sandwich"] ->
            Make.HandleSammich world
            |> Run

        | [ "grill"; "bacon" ] ->
            Grill.HandleBacon world
            |> Run

        | [ "grill"; "sammich" ]
        | [ "grill"; "sandwich" ] ->
            Grill.HandleSammich world
            |> Run

        | [ "unlock"; "door" ] ->
            Door.HandleUnlock world
            |> Run

        | [ "open"; "door" ] ->
            Door.HandleOpen world
            |> Option.iter Run

        | "take" :: tail ->
            Take.Handle tail world
            |> Run

        | [ "inventory" ] ->
            Inventory.Show world
            Run world

        | [ "turn"; "around" ] ->
            world
            |> HandleTurnAround
            |> Run

        | [ "walk" ] ->
            world
            |> HandleWalk
            |> Run

        | [ "give"; "up" ] ->
            MessageSink.WriteLine ConsoleColor.Green "I guess this game is just too much for you!"

        | [ "help" ] ->
            Help.Show()
            Run world
        | _ ->
            Music.Play Songs.Error
            MessageSink.WriteLine ConsoleColor.Red "For a list of commands, enter 'help'."
            Run world

