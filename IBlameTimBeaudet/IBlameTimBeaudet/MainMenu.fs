﻿namespace IBlameTimBeaudet

open System

module MainMenu =
    let rec Run () : unit =
        MessageSink.WriteLine ConsoleColor.White ""
        MessageSink.WriteLine ConsoleColor.Magenta "Main Menu:"
        MessageSink.Write ConsoleColor.Gray "    start"
        MessageSink.WriteLine ConsoleColor.DarkGray " - starts the game"
        MessageSink.Write ConsoleColor.Gray "    instructions"
        MessageSink.WriteLine ConsoleColor.DarkGray " - gives instructions on how to play the game"
        MessageSink.Write ConsoleColor.Gray "    quit"
        MessageSink.WriteLine ConsoleColor.DarkGray " - quits the game"

        match CommandSource.ReadCommand() with
        | [ "start" ] ->
            World.Create()
            |> Game.Run
            Run()
        | [ "quit" ] ->
            ()
        | [ "instructions" ] ->
            Instructions.Show()
            Run()
        | _ ->
            Music.Play Songs.Error
            MessageSink.WriteLine ConsoleColor.Red "Please enter 'start', 'instructions', 'quit'"
            Run()

